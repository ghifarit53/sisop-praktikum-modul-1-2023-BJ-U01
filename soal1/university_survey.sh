#!/bin/bash
echo -e "\n"
echo "Top 5 University in Japan :"
grep "Japan" nomor1.csv|
sort -k1 -n|
head -n5|
awk -F "," '{print $1, $2}'
echo -e "\n"

echo "Showing 5 the lowest FSR score University in Japan"
#!/bin/bash
grep "Japan" nomor1.csv|
sort -t "," -k9 -n|
head -n5|
awk -F "," '{print $2, $9}'
echo -e "\n"


echo "Finding top 10 University in Japan based on GER rank :"
#!/bin/bash
grep "Japan" nomor1.csv|
sort -t "," -k20 -n|
head -n10|
awk -F "," '{print $2, $20}'
echo -e "\n"

echo "Finding University with the 'keren' name"
#!/bin/bash
grep "Keren" nomor1.csv|
awk -F "," '{print $2}'