#!/bin/bash

b=ABCDEFGHIJKLMNOPQRSTUVWXYZ
s=abcdefghijklmnopqrstuvwxyz



time=$(date "+%H:%M %d:%m:%Y")
hour=$(echo "$time" | awk -F ':' '{print $1}')

file="./$time.txt"

hour=$((10#$hour+2))

touch "$file"

erg=$(cat /var/log/syslog)

erg=$(echo $erg | sed "y/$b$s/${b:$hour}${b::$hour}${s:$hour}${s::$hour}/")

echo "$erg" > "$file"


#Cronjob
# 0 */2 * * * /path-to-script/log_encrypt.sh
