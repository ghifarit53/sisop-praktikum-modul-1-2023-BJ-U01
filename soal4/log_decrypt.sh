#!/bin/bash

file=$1

if ! [[ -f "$file" ]]; then
  echo "File doesn't exist. Exiting...."
  exit 1
fi

if [[ ${file:0:2} == "./" ]]; then
  file=${file:2}  
fi

key=$(echo "$file" | awk -F ':' '{print $1}')
key=$((10#$key * -1 - 2))

b=ABCDEFGHIJKLMNOPQRSTUVWXYZ
s=abcdefghijklmnopqrstuvwxyz


text=$(cat "$file" | sed "y/$b$s/${b:key}${b::key}${s:key}${s::key}/")


echo "$text" > "$file"
