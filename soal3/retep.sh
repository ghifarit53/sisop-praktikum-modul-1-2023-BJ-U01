#!/usr/bin/env bash

users_list="users/users.txt"
log_file="log.txt"

# wrapper around echo to make loggin easier
# $1 user action
# $2 error level
# $3 message
function log {
  echo "$(date +'%y/%m/%d %H:%M:%S') $1: $2 $3" >> "$log_file"
}

function main {
  echo "Peter Griffin Login Page"
  echo "Enter your username and password"

  printf 'Username: '
  read username

  printf 'Password: '
  read -s password

  # check if user exist
  if [[ ! "$(grep "$username" "$users_list")" ]] ; then
    echo -e "\nIncorrect username or password"
    exit 1
  fi

  # check if password match
  if [[ "$(grep "$username" "$users_list" | cut -d ':' -f 2)" = "$password" ]] ; then
    echo -e "\nLogin success"
    log "LOGIN" "INFO" "User $username logged in"
  else
    echo -e "\nIncorrect username or password"
    log "LOGIN" "ERROR" "Failed attempt on user $username"
    exit 1
  fi
}

main
