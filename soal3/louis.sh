#!/usr/bin/env bash
# register program

users_list="users/users.txt"
log_file="log.txt"

mkdir -p "users"

# wrapper around echo to make loggin easier
# $1 user action
# $2 error level
# $3 message
function log {
  echo "$(date +'%y/%m/%d %H:%M:%S') $1: $2 $3" >> "$log_file"
}

function main {
  echo "Peter Griffin Registration System"
  echo "To begin, enter your new username and password"

  printf "Username: "
  read username

  printf "Password: "
  read -s password

  # check whether the username is already registered
  if [[ "$(grep "$username" "$users_list")" ]] ; then
    echo -e "\nUsername already exists, please try other username"
    log "REGISTER" "ERROR" "User already exists"
    exit 1
  fi

  # check whether password length is ≤ 8
  if [[ "${#password}" -lt 8 ]] ; then
    echo -e "\nPassword must be atleast 8 characters"
    exit 1
  fi

  # check whether password contains small and capital letter
  if [[ ! $(echo "$password" | grep '[a-z]' | grep '[A-Z]') ]] ; then
    echo -e "\nPassword must contain atleast one small letter and one capital letter"
    exit 1
  fi

  # check whether the password contains alphanumeric
  if [[ "$password" =~ [^a-zA-Z0-9] ]] ; then
    echo -e "\nPassword must only consists of alphanumeric characters"
    exit 1
  fi

  # check whether the password is the same as username
  if [[ "$password" = "$user" ]] ; then
    echo -e "\nPassword cannot be the same as username"
    exit 1
  fi

  # check whether the password contains 'chicken' or 'ernie'
  if [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]] ; then
    echo -e "\nPassword cannot contain the word 'chicken' or 'ernie' (case insensitive)"
    exit 1
  fi

  echo "$username:$password" >> "$users_list"
  echo -e "\nUsername succesfully created"
  log "REGISTER" "INFO" "User $username registered succesfully"
}

main
