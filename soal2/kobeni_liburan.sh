#!/usr/bin/env bash

# Cronjobs
# 0 */10 * * * cd /home/ghifarit53/sisop/modul-1/soal2 && ./kobeni_liburan.sh -d
# 0 0 * * * cd /home/ghifarit53/sisop/modul-1/soal2 && kobeni_liburan.sh -z

keyword="indonesia"
url="https://loremflickr.com/320/240/$keyword"

function batch_download {
  hour="$(date +'%H')"
  hour="$((10#$hour))"

  # if time is 00.xx, download 1
  if [[ "$hour" -eq 0 ]] ; then
    wget "$url" -O "perjalanan_1.jpg" -q
  else
    for i in $(seq 1 "$hour") ; do
      echo "Downloading perjalanan_${i}.jpg"
      wget "$url" -O "perjalanan_${i}.jpg" -q
    done
  fi

  # for keeping track of folder number
  # 1. create temporary file for storing
  #    folder counter
  counter_file="folder_counter.txt"
  if [[ ! -f "$counter_file" ]] ; then
    echo 1 > "$counter_file"
  fi

  # 2. read the file if it's already exist
  if [[ -f "$counter_file" ]] ; then
    cnt="$(cat "$counter_file")"
  fi

  # 3. check if dir is already exist,
  #    if it is, then mkdir and
  #    increment the counter
  if [[ ! -d "kumpulan_${cnt}" ]] ; then
    mkdir -p "kumpulan_${cnt}"
    echo "$((cnt + 1))" > "$counter_file"
  fi

  # move all the images to the folder
  mv perjalanan_*.jpg "kumpulan_${cnt}/"
}

function zip_folder {
  # for keeping track of zip number
  # most parts are copied from folder number counter
  # in batch_download()
  counter_file="zip_counter.txt"
  if [[ ! -f "$counter_file" ]] ; then
    echo 1 > "$counter_file"
  fi

  if [[ -f "$counter_file" ]] ; then
    cnt="$(cat "$counter_file")"
  fi

  if [[ ! -f "devil_${cnt}.zip" ]] ; then
    # -m: remove original file after successfuly zipping
    # -r: recursive (to be able to zip a folder)
    zip -r "devil_${cnt}.zip" kumpulan_*
    echo "$((cnt + 1))" > "$counter_file"
  fi
}

case "$1" in
  -d) batch_download ;;
  -z) zip_folder ;;
  *) echo "Invalid option! Use -d or -z" ; exit 1 ;;
esac
