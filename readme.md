# sisop-praktikum-modul-1-2023-BJ-U01

Group Members:
1. Mikhael Aryasatya Nugraha (5025211062)
2. Muhammad Dzaky Farhan (5025211069)
3. Muhammad Ghifari Taqiuddin (5025211063)

## Number 1
### This task has 4 parts that needs to be done :

A. Finding the top 5 university in japan<br>
B. Finding the lowest FSR score based on number 1<br>
C. Finding the top 10 emplyment outcome by sorting based on GER rank<br>
D. Finding the university with "keren" keywords<br>

### A. Finding the top 5 university in japan

```bash
grep "Japan" nomor1.csv|
sort -k1 -n|
head -n5|
awk -F "," '{print $1, $2}'
```

```bash
grep "Japan" nomor1.csv|
```

Search the keyword japan on nomor1.csv file <br>

```bash
sort -k1 -n|
```

Sorting from column number 1 on the nomor1.csv file <br>

```bash
head -n5|
```

Show only top 5 university

```bash
awk -F "," '{print $1, $2}'
```

Printing the column 1 which is the rank and column 2 The university name.

### B. Finding the lowest FSR score based on number 1

```bash
sort -t "," -k9 -n|
```

Sorting from column number 9 which are the column for FSR

```bash
head -n1|
```

Show only 1 university from top 5 university

```bash
awk -F "," '{print $2, $9}'
```

Printing column number 2 Which is the university name and number 9 for the FSR rank

### C. Finding the top 10 emplyment outcome by sorting based on GER rank

```bash
sort -t "," -k20 -n|
```

Sorting from column number 20 (GER Rank)

```bash
head -n10|
```

Showing top 10 university

```bash
awk -F "," '{print $2, $20}'
```

Printing the University name and the GER Rank

### D. Finding the university with "keren" keywords

```bash
grep "Keren" nomor1.csv|
awk -F "," '{print $2}'
```

Finding the keyword "Keren" in file nomor1.csv <br>
Printing the University name

### Program output
![Screenshot_2023-03-10_at_20.12.12](/uploads/f87940a3ea69c4ac9f39a661c88c9474/Screenshot_2023-03-10_at_20.12.12.png)

## Number 2
> Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia.
> Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x,
> Kobeni tidak dapat melakukan hal tersebut

There are two objectives of the problem in number 2
- To download `n` image about Indonesia every 10 hours, `n` is the hour
  when the downloading process should occur. For example, if the download occur
  at 09.00 AM, then 9 images must be downloaded, with the name format `perjalanan_i.jpg`,
  where `i` is from 1 to `n`. After done downloading, move all the
  images into a folder named `kumpulan_XX/`, where `XX` is a sequence of number starting from 1
- To zip all the folders `kumpulan_XX/` into `devil_XX.zip` every day.

A random image API service called loremflickr will be used, it
can find random image based on keywords
```bash
#!/usr/bin/env bash

keyword="indonesia"
url="https://loremflickr.com/320/240/$keyword"
```

Next, create two functions, the first one is the `batch_download()`
function, which is for downloading the images according to the
problem description.

```bash
function batch_download {
  # get current hour, and remove leading zero
  # when the clock is 00-09
  hour="$(date +'%H')"
  hour="$((10#$hour))"

  # if time is 00.xx, download 1
  if [[ "$hour" -eq 0 ]] ; then
    wget "$url" -O "perjalanan_1.jpg"
  else
  # otherwise download n times
    for i in $(seq 1 "$hour") ; do
      wget "$url" -O "perjalanan_${i}.jpg"
    done
  fi

  # for keeping track of folder number
  # 1. create temporary file for storing
  #    folder counter
  counter_file="folder_counter.txt"
  if [[ ! -f "$counter_file" ]] ; then
    echo 1 > "$counter_file"
  fi

  # 2. read the file if it's already exist
  if [[ -f "$counter_file" ]] ; then
    cnt="$(cat "$counter_file")"
  fi

  # 3. check if dir is already exist,
  #    if it is, then mkdir and
  #    increment the counter
  if [[ ! -d "kumpulan_${cnt}" ]] ; then
    mkdir -p "kumpulan_${cnt}"
    echo "$((cnt + 1))" > "$counter_file"
  fi

  # move all the images to the folder
  mv perjalanan_*.jpg "kumpulan_${cnt}/"
}

```

Then, the second function is the `zip_folders()` function, to
collect all the downloaded folders batch into a single zip
file everyday

```bash
function zip_folders {
  # for keeping track of zip number
  # most parts are copied from folder number counter
  # in batch_download()
  counter_file="zip_counter.txt"
  if [[ ! -f "$counter_file" ]] ; then
    echo 1 > "$counter_file"
  fi

  # if the file is already exist, just read from it
  if [[ -f "$counter_file" ]] ; then
    cnt="$(cat "$counter_file")"
  fi

  if [[ ! -f "devil_${cnt}.zip" ]] ; then
    # -m: remove original file after successfuly zipping
    # -r: recursive (to be able to zip a folder)
    zip -r "devil_${cnt}.zip" kumpulan_*
    echo "$((cnt + 1))" > "$counter_file"
  fi
}

```

But the problem states that we can only use a single script
for two purpose, so a command flag can be used here, using
bash switch case statement

```bash
# command flag so the same script
# can be run for two different function
case "$1" in
  -d) batch_download ;;
  -z) zip_folders ;;
  *) echo "Invalid option! Use -d or -z" ; exit 1 ;;
esac
```

And finally to run the script every 10 hours to download image,
the cronjob is as follows

```
0 */10 * * * cd /home/ghifarit53/sisop/modul-1/soal2 && ./kobeni_liburan.sh -d
```

And for zipping the folders every day, the cron is as follows

```
0 0 * * * cd /home/ghifarit53/sisop/modul-1/soal2 && kobeni_liburan.sh -z
```

Here's the output result of `kobeni_liburan.sh -d`

![Screenshot_2023-03-10_at_21.19.12](/uploads/fe9e7336be73e9f3324a558fa619d534/Screenshot_2023-03-10_at_21.19.12.png)

And for `kobeni_liburan.sh -z`

![Screenshot_2023-03-10_at_21.19.20](/uploads/7896a1dd13d8051caab8ea51e9f55716/Screenshot_2023-03-10_at_21.19.20.png)

And here's some of the downloaded images and zipped folder

![Screenshot_2023-03-10_at_21.30.54](/uploads/e60760c38dcc81d4f1fca449e876c78b/Screenshot_2023-03-10_at_21.30.54.png)

## Number 3
> Peter Griffin hendak membuat suatu sistem register pada script `louis.sh`
> dari setiap user yang berhasil didaftarkan di dalam file `/users/users.txt`.
> Peter Griffin juga membuat sistem login yang dibuat di script `retep.sh`

### This problem has 2 parts to be solved
1. Create a registration system in `louis.sh`, with the password is required
   to fulfill certain condition. All the user credentials is stored in `users/users.txt`
2. Create a login system in `retep.sh`

Also some of the login and registration action are required to be logged
into a log file `log.txt`

The first one is the registration system. There is several requirements for the password
- 8 characters minimum
- atleast contain 1 small letter and 1 capital letter
- only consist of alphanumeric characters
- cannot be the same as the username
- cannot contain the word 'chicken' or 'ernie'

If any of the one above is not fulfilled, do an early exit to prevent
later error in the system

```bash
#!/usr/bin/env bash
# register program

users_list="users/users.txt"
log_file="log.txt"

mkdir -p "users"

# wrapper around echo to make logging easier
# $1 user action
# $2 error level
# $3 message
function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') $1: $2 $3" >> "$log_file"
}

function main {
  echo "Peter Griffin Registration System"
  echo "To begin, enter your new username and password"

  printf "Username: "
  read username

  printf "Password: "
  read -s password

  # check whether the password is the same as username
  if [[ "$password" = "$user" ]] ; then
    echo -e "\nPassword cannot be the same as username"
    exit 1
  fi

  # check whether the username is already registered
  if [[ "$(grep "$username" "$users_list")" ]] ; then
    echo -e "\nUsername already exists, please try other username"
    log "REGISTER" "ERROR" "User already exists"
    exit 1
  fi

  # check whether password length is ≤ 8
  if [[ "${#password}" -lt 8 ]] ; then
    echo -e "\nPassword must be atleast 8 characters"
    exit 1
  fi

  # check whether password contains small and capital letter
  if [[ ! $(echo "$password" | grep '[a-z]' | grep '[A-Z]') ]] ; then
    echo -e "\nPassword must contain atleast one small letter and one capital letter"
    exit 1
  fi

  # check whether the password contains alphanumeric
  if [[ "$password" =~ [^a-zA-Z0-9] ]] ; then
    echo -e "\nPassword must only consists of alphanumeric characters"
    exit 1
  fi

  # check whether the password contains 'chicken' or 'ernie'
  if [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]] ; then
    echo -e "\nPassword cannot contain the word 'chicken' or 'ernie' (case insensitive)"
    exit 1
  fi

  echo "$username:$password" >> "$users_list"
  echo -e "\nUsername succesfully created"
  log "REGISTER" "INFO" "User $username registered succesfully"
}

main
```

And then, below is the login program `retep.sh`

```bash
#!/usr/bin/env bash

users_list="users/users.txt"
log_file="log.txt"

# wrapper around echo to make loggin easier
# $1 user action
# $2 error level
# $3 message
function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') $1: $2 $3" >> "$log_file"
}

function main {
  echo "Peter Griffin Login Page"
  echo "Enter your username and password"

  printf 'Username: '
  read username

  printf 'Password: '
  read -s password

  # check if user exist
  if [[ ! "$(grep "$username" "$users_list")" ]] ; then
    echo -e "\nIncorrect username or password"
    exit 1
  fi

  # check if password match
  if [[ "$(grep "$username" "$users_list" | cut -d ':' -f 2)" = "$password" ]] ; then
    echo -e "\nLogin success"
    log "LOGIN" "INFO" "User $username logged in"
  else
    echo -e "\nIncorrect username or password"
    log "LOGIN" "ERROR" "Failed attempt on user $username"
    exit 1
  fi
}

main
```

Here's the program output that shows the complete process from registration to login

![Screenshot_2023-03-10_at_21.23.50](/uploads/3f4864414c7e7f7bc04b0bbc83a04210/Screenshot_2023-03-10_at_21.23.50.png)

**REVISION**
- The year format didn't match the format in the problem statement. Currently it's `YYYY/MM/DD`
  when it should be `YY/MM/DD`. Changing the flag in `date` command from `%Y` to `%y` solved the problem
- Need to rearrange the `if-else` statements. Currently, some parts doesn't give expected output
  when being tested with various username-password inputs. (e.g. first the system should check
  whether the username is the same as password, irrelevant to their length)

## Number 4
### This task has three parts that needs to be done :
1. Create Encryption Script log_encrypt.sh encrypted with Caesarian Shift based on the time (hour) of the script executed
2. Create Decryption Script log_decrypt.sh based on the Encryption Script
3. Backup syslog files every 2 hours

### 1. Encryption Script

The first part of this task is to make the script to encrypt the syslog files.
This uses a cipher called the Caesarian Shift, where letters would be substituted
by another letter based on special rules. The rules of the substitution
are based on a computer’s system clock where the letters will shift
by `the hour of the day the script is executed + 2`.
If the shift goes further than the letter `z`, it will go back to `a`
until it loops back to the chosen letter.

The first command used to fulfill this rule is `date`. This command
allows the ability to get the system time automatically when executed.
For the purpose of this task, there are a few arguments needed, shown by `$time` below.

```bash
time=$(date "+%H:%M %d:%m:%Y")
```

The variable `$time` is used as a format to save the ecrypted text as a `.txt` file.
To do that, `$time` and `.txt` are put into `$file` and a new file is created with touch.

```bash
#Not in the exact order of the script
file="./$time.txt"

touch "$file"
```

Then, to extract only the `hour` from `$time`, `awk` is used to get the characters before the colon.

```bash
hour=$(echo "$time" | awk -F ':' '{print $1}')
```

This allows the script to use the value for the shift encryption, later adding the value to match the rule.

```bash
hour=$((10#$hour+2))
```

Here is shown the process of fulfilling the `hour + 2` rule.
There was a problem with this process before with the previous iteration of the script.

```bash
hour=$(($hour+2)) #previous iteration
```

Apparently, using the output of `date +%H` produces an error
whenever the output has a leading zero in which
the command will have in the mornings
(for example: 09.00 AM will output 09).
```
Script error: value too great for base (error token is "09")
```

Numbers that have a leading zero such as 01, 02, 03
are interpreted as an octal in bash, which means the value
goes from 0-7. Consequently, keys from 6 AM to 9:59 AM will
cause the script to malfunction. To handle this, `10#`
is added before `$hour` to turn the value from an octal
to decimal as already shown above.


The second command used to fulfill the rule is the ‘sed’ command. One of
this command’s uses is to transform string by a certain pattern
which will be demonstrated as the following:
```bash
#Capital and non-capital letters
b=ABCDEFGHIJKLMNOPQRSTUVWXYZ
s=abcdefghijklmnopqrstuvwxyz

#The sed command
sed "y/$b$s/${b:$hour}${b::$hour}${s:$hour}${s::$hour}/"
```

The `y` in `sed` is the one that allows the text to be transformed.
It transforms the text inside the space between the first 2
forward slashes (in the case of the snippet above, it’s `$b`
for capital letters and `$s` for non-capital letters) by the
pattern shown by the space between the second and the third
forward slashes.

```bash
The pattern :
${b:$hour} → get the string from $b from index $hour to the last index
Example :
Assuming $hour=19 :
${b:$hour} = TUVWXYZ

${b::$hour} → get the string from the first index to one index before $b
Example :
Assuming $hour=19 :
${b::$hour} = ABCDEFGHIJKLMNOPQRS

Therefore,
${b:$hour}${b::$hour} = TUVWXYZABCDEFGHIJKLMNOPQRS

The same pattern can be said for $s but for non capital letters
```

When fed an input of text like the snippet below,

```bash
erg=$(echo $erg | sed "y/$b$s/${b:$hour}${b::$hour}${s:$hour}${s::$hour}/")
```

The command will transform every instance of the alphabet to its
caesarian shift. This is used to encrypt the syslog files that
can be obtained by using the `cat` command with the syslog
files being used as an input to the command above.

```bash
erg=$(cat /var/log/syslog) #Before being used as an input for sed
```


The last thing is to put the encrypted text inside the newly created
file. Since the file is still empty, another command is used to
fill in the file with the encrypted text.

```bash
echo "$erg" > "$file"
```

The contents of the encrypted text is printed, but then is redirected
to the file using `>` which fulfills the task.

### 2. Decryption Script
To decrypt the caesarian shift, one must find the key that encrypts
the text. Luckily, this is but a simple feat since the key is
dangling by the format of the `.txt` file, containing the time
the encryption was done.

The Format:

`hour:minute day:month:year.txt`

But first, the script is made so that it needs an extra argument
to run, being the encrypted file. The script won’t run if the user
doesn’t supply the script a file through arguments.

```bash
file=$1

if ! [[ -f "$file" ]]; then
  echo "File doesn't exist. Exiting...."
  exit 1
fi
```

Also, if the script contains the characters `./` (usually used
in executions in linux as the first and second character if the
script is located in the same folder. This means the log files
can only be decrypted if they are in the same folder as the script. As the location of the decryption file doesn't matter, it is left alone), it will be deleted and not used by using this statement.

```bash
if [[ ${file:0:2} == "./" ]]; then
  file=${file:2}
fi
```

Next, to extract the hour from the file for the key, the commands below are used :

```bash
key=$(echo "$file" | awk -F ':' '{print $1}')
```

`$file` is used as an input to `awk`, which in this case is to get the string
before the first colon as shown above. Then to decrypt, the rules for encryption
need to be reversed. The alphabet is shifted backwards instead of forwards.

```bash
key=$((10#$key * -1 - 2)) #A negative number
```

```
Example assuming the hour is 5:
UNSHIFT 5
F → A
E → Z
D → Y
Etc.
```

Everything else after is similar to the encryption process with the
difference of accessing the strings' index backwards with a negative number.

```bash
b=ABCDEFGHIJKLMNOPQRSTUVWXYZ
s=abcdefghijklmnopqrstuvwxyz

text=$(cat "$file")
text=$(echo "$text" | sed "y/$b$s/${b:key}${b::key}${s:key}${s::key}/")

echo "$text" > "$file"
```

And with the last command being the redirection of the text to the file, overwriting it, the decryption task is finally done.<br><br>

### 3. Backing up syslog every 2 hours

To back up syslog every 2 hours, `cron` is used to execute the encryption
script every 2 hours since it makes a copy every time it is ran.

```
crontab -e
```

This command is used to open the crontab, containing all of the cronjobs. To execute the file every 2 hours, the command below is added to the cron file.

```
0 */2 * * * /path-to-script/log_encrypt.sh
```

After saving and closing the cron file, a new encrypted copy will be
added to the folder containing the script every 2 hours.


### 4. Script Execution Screenshots<br>

![executingEncrypt](/uploads/dac5b2327a66b57557ec4e19e489bf97/executingEncrypt.png)
<br>Execution of log_encrypt.sh. A new file based on the format is generated.<br><br>

![catEncSnippet](/uploads/dda54cfb315368a8852d3f28c22533b6/catEncSnippet.png)
<br>A snippet of the encrypted text (encrypted at 7 PM). Read by using the `cat` command.<br><br>

![executingDecrypt](/uploads/9d4c7370d427a3d2286d4f75c35521fc/executingDecrypt.png)
<br>Execution of log_decrypt.sh using the previously encrypted file. The file is overwritten by the decryption process.<br><br>

![catDecrSnippet](/uploads/7bac10a3d23b343ca844ae2866de5d6b/catDecrSnippet.png)
<br>A snippet of the text decrypted by the script, read by using the `cat` command.
